;This is an AHK script that is designed to make using a Model M keyboard usable on a modern Windows system. This runs under the assumption that you are on Windows (the only platform that AHK is currently supported and available on) and are using an official IBM Model M keyboard that is missing a Windows key. Of course, you can run this script with any other keyboard but this is designed to make an IBM Model M more "equal" to a modern keyboard in function.

;Turns the Caps Lock key into F13 for Push-To-Talk Functionality
CapsLock::F13

;Sets LCtrl+LAlt+L to lock the screen
<^<!l::
Run, rundll32.exe user32.dll`, LockWorkStation
return

;Turn Left Alt + F1 into a "Windows Key"
<!F1::
Keywait, LAlt
Keywait, F1
Send {LWin}
return

;LeftCtrl+LeftAlt+R will now open a Run dialogue to make opening programs easier.
<^<!R::
Keywait, LCtrl
Keywait, LAlt
Keywait, r
Run C:\Users\ultima\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\System Tools\Run
return

;Enables "return to desktop" or Win+d command
<^<!D::
Send #d
return

;Enables moving Windows between monitors with LCtrl+LAlt+Arrow Left or Right, Maximizes/Minimizes with Up/Down. Note that on multiple monitors of differing sizes, this may cause a window to change size when moving to another monitor.

<^<!Up::
    SendInput, #{Up}
return

<^<!Down::
    SendInput, #{Down}
return

<^<!Left::
    SendInput, #+{Left}
return

<^<!Right::
    SendInput, #+{Right}
return

;Puts Window Snapping on LCtrl+LAlt+Shift+Arrow Left or Right.
<^<!<+Left::
     SendInput, #{Left}
return

<^<!<+Right::
     SendInput, #{Right}
return