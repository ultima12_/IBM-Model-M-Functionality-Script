This project is simple, it's an AutoHotKey script that can be compiled as a portable .exe file which attempts to re-create 
several functions available on a modern keyboard that may not be available on certain IBM Model M keyboards. As this is my keyboard
of choice, this is a script that is heavily personalized to my personal computing habits as well as my own desires. This script may
or may not include every function you need, and may or may not be written well. I will try to credit any contributors should any occur,
but most of the things done in this script are easily available to the public. This is just a ready-made version of my own personal
features presented in a way that is easy to access.

This README will later be updated with what keys do what, for now examining the script will give you comments that try to explain
in brief what each section is about.

This project may or may not see frequent droughts of development, but anyone is welcome to copy the script and add their own features
or remove features I have presented. You are free to distribute this as you wish, but please do not attempt to do anything malicious
with this kind of script. I just wanted something that worked for me, and to help others. Please do the same.